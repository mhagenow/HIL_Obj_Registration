#!/bin/bash

# Mike Hagenow
# 12/30/21
# Compile script for building cpp fitting

cd src/affordance_corrections/nodes/affordance_corrections/affordance_helpers/cppfitting
rm -r build
mkdir build
cd build
cmake ..
make
