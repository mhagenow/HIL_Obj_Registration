#!/usr/bin/env python3
# File name: ros_affordance_wrapper.py
# Description: Creates affordance engine and deals with
# passing of information between the input and engine interfaces
# Author: Mike Hagenow
# Date: 6/17/2021

import argparse
import time
import open3d as o3d
import numpy as np
import trimesh
import rospy
import rospkg
from scipy.spatial.transform import Rotation as ScipyR

from affordance_corrections.ros_affordance_wrapper import ROSAffordances
from affordance_corrections.affordance_engine import AffordanceEngine
from affordance_corrections.affordance_helpers.rviz_helpers import pcd_to_pc2, mesh_to_marker, circ_marker
from affordance_corrections.affordance_helpers.InputHandler import inputLaunch
from affordance_corrections.affordance_helpers.InteractiveMesh import InteractiveMesh

from geometry_msgs.msg import PointStamped, Quaternion, PoseStamped
from visualization_msgs.msg import Marker, MarkerArray
from sensor_msgs.msg import PointCloud2
from geometry_msgs.msg import Twist, Pose
from std_msgs.msg import String, Float64

from interactive_markers.interactive_marker_server import *

import threading


class ROSMarkerAffordances(ROSAffordances):
    """ Keeps track of objects of interest, fits, etc. Specificially, provides a level on top of the
        affordance engine which interfaces with many ROS topics"""
    def __init__(self):
        ''' Initalize other engines and set up ROS topics'''
        self.server = InteractiveMarkerServer("interactive_meshes")
        self.int_markers = []
        rospy.Subscriber("posecorrection", PoseStamped, self.applyPose)
        rospy.Subscriber("modelchange",String,self.applyModel)
        self.triggerPub = rospy.Publisher('rviz_triggers', String, queue_size=1)
        self.pose_update_pub = rospy.Publisher('posecorrection', PoseStamped, queue_size=1)
        self.model_update_pub = rospy.Publisher('modelchange',String,queue_size=1)
        self.rviztrig_pub = rospy.Publisher('rviz_triggers',String,queue_size=1)
        self.int_articulationpub = rospy.Publisher('int_articulation',Float64,queue_size=1)
        self.int_correctionpub = rospy.Publisher('int_correction',Pose,queue_size=1)
        super().__init__("interactive_marker")

    def applyAngle(self,theta):
        ''' Take a single angle articulation and apply it to the current pose of the active object'''

        self.updateNeeded= True
        self.timer = 0.0
       
        self.engine.angle_active_object(theta)

        if len(self.engine.objectsOfInterest)>0:
            active_obj = self.engine.active_obj
            angles = self.engine.objectsOfInterest[active_obj].fits[self.engine.objectsOfInterest[active_obj].active_id].angles
            if len(angles)>0:
                self.int_markers[active_obj].updateIntMarkerAngle(angles[0])
        
        # update visualization
        # worldState = self.engine.getUpdatedWorld()
        # self.updateVisualization(worldState, sceneChange=False, changing=True)

    def applyPose(self,data):
        ''' After a marker has been moved -- send the final pose to the backend'''
        self.updateNeeded= True
        self.timer = 0.0
        
        obj_id = int(data.header.frame_id)
        self.engine.setActiveObject(obj_id)
        
        pos = np.zeros((3,))
        pos[0] = data.pose.position.x
        pos[1] = data.pose.position.y
        pos[2] = data.pose.position.z

        quat = np.zeros((4,))
        quat[0] = data.pose.orientation.x
        quat[1] = data.pose.orientation.y
        quat[2] = data.pose.orientation.z
        quat[3] = data.pose.orientation.w

        rot = ScipyR.from_quat(quat).as_matrix()

        self.engine.update_object_pose(pos,rot,obj_id)
       
        # update visualization
        # worldState = self.engine.getUpdatedWorld()
        # self.updateVisualization(worldState, sceneChange=False, changing=True)

    def applyModel(self,data):
        ''' if the model is switched - propogate to the back end '''
        str_temp = data.data.split(",")
        obj_id = int(str_temp[0])
        model_id = int(str_temp[1])

        self.engine.objectsOfInterest[obj_id].active_id = model_id

        str_temp = String()
        str_temp.data = 'change_model'
        self.clickedptlogger.publish(str_temp)
        
        # update visualization
        worldState = self.engine.getUpdatedWorld()
        self.updateVisualization(worldState, sceneChange=False, changing=False)
    
    def clearAllPts(self):
        ''' removes all tracked objects of interest and clears marker array topic'''
        num_markers = len(self.pts_of_interest)
        self.pts_of_interest.clear()
        self.engine.objectsOfInterest.clear()
        self.engine.active_obj = 0
        self.server.clear()
        self.server.applyChanges()
        
        worldState = self.engine.getUpdatedWorld()
        self.updateVisualization(worldState, sceneChange=False, changing=False)

    def triggers(self,data):
        ''' Captures triggering events (strings) from the rviz front end (buttons, etc)'''
        if data.data=="clear":
            self.clearAllPts()
        if data.data=="int_marker_updates":
            # not done changing -- don't update yet
            self.updateNeeded= True
            self.timer = 0.0
        if data.data=="deleteactive":
            self.deleteActive()
        if "angle:" in data.data:
            new_angle = float(data.data.split(":")[1])/100.0
            ang_temp = Float64()
            ang_temp.data = new_angle
            self.int_articulationpub.publish(ang_temp)
            self.applyAngle(new_angle)
        if data.data=="refittingon":
            str_temp = String()
            str_temp.data = 'refittingon'
            self.clickedptlogger.publish(str_temp)
            self.engine.setActiveObjectRefitting(True)
        if data.data=="refittingoff":
            str_temp = String()
            str_temp.data = 'refittingoff'
            self.clickedptlogger.publish(str_temp)
            self.engine.setActiveObjectRefitting(False)

    def updateVisualization(self, worldState, sceneChange=True, changing=False):
        ''' Use Engine world to update visualization
        Note: worldState is a dictionary '''

        

        scene = worldState.get('scene')
        objectsOfInterest = worldState.get('objectsOfInterest')
        active_obj = worldState.get('active_obj')

        # get rid of interactive markers temporarily
        self.lock.acquire()
        self.server.clear()
        self.server.applyChanges()
        self.lock.release()

        # see whether any temporary markers need to be removed
        self.ptsInterestVis(worldState)

        # Scene is not always published (since it can be large)
        if sceneChange and scene is not None:
            pc2 = pcd_to_pc2(scene,self.active)
            self.scenepub.publish(pc2)

        self.lock.acquire()
        self.int_markers.clear()
        self.lock.release()
        # publish each one of the meshes in the affordanceEngine
        for ii in range(0,len(objectsOfInterest)):
            active_id = objectsOfInterest[ii].active_id
            models = objectsOfInterest[ii].models

            color = np.array([0.5, 0.5, 0.5]) # gray for inactive
            if changing and ii==self.engine.active_obj:
                color = np.array([0.5, 0.8, 0.5]) # mod. green for active, but changing
            elif ii==self.engine.active_obj:
                color = np.array([0.2, 0.8, 0.2]) # green for active
            
            self.int_markers.append(InteractiveMesh(self.lock,models,self.server,self.pose_update_pub,self.model_update_pub,self.rviztrig_pub,self.int_correctionpub,objectsOfInterest[ii].fits,objectsOfInterest[ii].sorted,ii, active_id,color))
        
        # If there is an angle for the model, publish it to rviz for the slider
        if len(objectsOfInterest)>0 and changing is False:
            model_id_for_active = objectsOfInterest[self.engine.active_obj].active_id
            if len(objectsOfInterest[self.engine.active_obj].models[model_id_for_active].joints)>0:
                str_temp = String()
                angle_norm = objectsOfInterest[self.engine.active_obj].fits[model_id_for_active].angles[0]
                min_angle = objectsOfInterest[self.engine.active_obj].models[model_id_for_active].joints[0].min_angle
                max_angle = objectsOfInterest[self.engine.active_obj].models[model_id_for_active].joints[0].max_angle
                angle_norm = int((angle_norm-min_angle)/(max_angle-min_angle)*100.0)
                str_temp.data = "anglerviz:"+str(angle_norm)
                self.triggerPub.publish(str_temp)

def main():
    rospy.init_node('marker_affordance_specifications', anonymous=True)
    rospack = rospkg.RosPack()
    root_dir = rospack.get_path('affordance_corrections')+'/../../'

    # Create instance
    rosmarkaff = ROSMarkerAffordances()
    rosmarkaff.setFitting(True)
    rosmarkaff.toggleSVDforInitialArticulation(True)
    rosmarkaff.setCppFitting(False)

    rosmarkaff.setScene(root_dir+'pcd/study3.pcd')
    rosmarkaff.setModels([root_dir+'src/affordance_models/urdfs/large_blue_valve.urdf', root_dir+'src/affordance_models/urdfs/small_blue_valve.urdf', root_dir+'src/affordance_models/meshes/e_stop.STL', root_dir+'src/affordance_models/meshes/handle.STL', root_dir+'src/affordance_models/urdfs/yellow_valve.urdf'])
    rosmarkaff.runLoop()
if __name__ == '__main__':
    main()
