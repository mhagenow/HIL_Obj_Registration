#include <stdio.h>

extern "C" {
    void myprint()
    {
        printf("\n\nThis is where we will do the fitting!\n\n");
    }

    // Before we go too far down the c++ rabbit hole, I would design a fake loop here
    // and test that openMP works (that we can parallelize across at least 5 threads)
    // the fake loop can just sum values from 0 to 10000 (any task that takes a while so you can
    // (get a sense of whether the parallelization is working) -- you can time it back in the python script
}